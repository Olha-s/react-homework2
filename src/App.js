import React, {Component} from 'react';
import './App.css';
import Modal from './components/Modal/Modal';
import ListItems from "./components/ListItems/ListItems";


class App extends Component {
    state = {
        items: [],
        modalShow: false
    };

    modalOpen = () => {
        this.setState({modalShow: true});
    };
    modalClose = () => {
        this.setState({modalShow: false});
    };

    componentDidMount() {
        this.getItems()
    }

    getItems() {
        fetch('./items.json').then(r => r.json())
            .then(data => this.setState({items: data}));
    };

    render() {
        const {items} = this.state;
        return (
            <div className="App">
                <ListItems items={items} onClick={this.modalOpen}/>
                {this.state.modalShow && <Modal
                    modalOne={true}
                    onClick={this.modalClose}
                    header={"Adding to cart"}
                    closeButton={true}
                    text={"The item has been successfully added to the cart"}
                    actions={
                        <>
                            <button className={"Modal__btn-1"}>Ok</button>
                            <button className={"Modal__btn-1"}>Cancel</button>
                        </>
                    }
                />}
            </div>
        );
    }
}

export default App;
