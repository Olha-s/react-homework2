import React, {Component} from 'react';
import {star} from '../../images/star';
import PropTypes from "prop-types";

class Icon extends Component {
    state = {
        starShowFilled: false,
    };

    starFilled = () => {
        if (this.state.starShowFilled) {
            this.setState({starShowFilled: false})
            localStorage.removeItem(`Add to favorites ${this.props.name}`)
        } else {
            this.setState({starShowFilled: true});
            localStorage.setItem(`Add to favorites ${this.props.name}`, `article: ${this.props.article}`)
        }
    };

    render() {
        return (
            <div onClick={this.starFilled}>
                {star(this.state.starShowFilled)}
            </div>
        );
    }
}

Icon.propTypes = {
    name: PropTypes.string,
    article: PropTypes.string,
};

export default Icon;