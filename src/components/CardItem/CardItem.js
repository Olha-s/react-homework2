import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './CardItem.scss';
import Icon from "../Icon/Icon";

class CardItem extends Component {
    onBtnClick = () => {
        this.props.onClick();
        localStorage.setItem(this.props.card.name, 'Item add to cart')
    };

    render() {
        const {card: {name, price, src, article, color}} = this.props;

        return (
            <div className="card">
                <Icon article={article} name={name}/>
                <div className="card__img-wrapper">
                    <img className="card__img" src={src} alt=""/>
                </div>
                <div className="card__info-wrapper">
                    <p className="card__name">{name}</p>
                    <p className="card__article">Артикль {article}</p>
                    <p className="card__color">цвет: {color}</p>
                    <div className="card__footer">
                        <p className="card__price">{price}</p>
                        <button onClick={this.onBtnClick}>Add to cart</button>
                    </div>
                </div>
            </div>
        );
    }
}

CardItem.defaultProps = {
    card: {
        color: 'по запросу'
    }

};
CardItem.propTypes = {
    card: PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.string,
        src: PropTypes.string,
        article: PropTypes.string,
        color: PropTypes.string,
        onClick: PropTypes.func
    })
};


export default CardItem;
