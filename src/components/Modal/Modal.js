import React, {Component} from 'react';
import './Modal.scss';
import cross from '../../images/cross.png';
import PropTypes from "prop-types";

class Modal extends Component {

    render() {
        const {onClick} = this.props;
        const wrapperClose = (e) => {
            if (e.target.getAttribute('class') === "ModalWrapper") {
                onClick();
            }
        };
        return (
            <div className="ModalWrapper" onClick={wrapperClose}>
                <div className="Modal">
                    <div className="Modal__header">
                        <p className="Modal__header-text">{this.props.header}</p>
                        <span><img src={cross} alt=""/></span>
                    </div>
                    <div>
                        <p className="Modal__main">{this.props.text}</p>
                        <div className="Modal__buttons">{this.props.actions}</div>
                    </div>
                </div>
            </div>
        );
    }
}


Modal.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    src: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
    onClick: PropTypes.func
};

export default Modal;