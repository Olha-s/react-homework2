import React, {Component} from 'react';
import CardItem from "../CardItem/CardItem";
import PropTypes from 'prop-types';
import './ListItems.scss'

class ListItems extends Component {
    render() {
        const {items, onClick} = this.props;
        const cards = items.map(el => <CardItem card={el} key={el.article} onClick={onClick}/>);
        return (
            <div className="ListItems">
                {cards}
            </div>
        );
    }
}

ListItems.propTypes = {
    card: PropTypes.array,
    onClick: PropTypes.func
};


export default ListItems;